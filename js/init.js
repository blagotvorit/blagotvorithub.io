"use strict";

var App = function(){

  function init(){
    svg4everybody();
    _initMainMenu();
    _activeOffset();
    // yaMapContacts();
  }

  function isTouch(){
    return ('ontouchstart' in document.documentElement) ? true : false;
  }

     /*******   yandex карта в контактах    *******/


  function _initMainMenu(){
      _openDropdownMainMenu();
  } 
  /******** выпадающее меню   ***********/

  function _openDropdownMainMenu(){
      var menu,
          dropdowns,
          fix_menu,
          fix_drop;

      menu = document.querySelector(".nav-main");
      if(!menu) {return;}
      dropdowns = menu.querySelectorAll(".nav-main .dropdown");

      [].forEach.call(dropdowns, function(element, index){
          if(isTouch()){
              element.addEventListener('touchstart', toggleDropdown, false);
              element.addEventListener('click', toggleDropdown, false);
          } else {
              element.addEventListener('mouseenter', toggleDropdown, false);
              element.addEventListener('mouseleave', toggleDropdown, false);
          }            
      });

      fix_menu = document.querySelector(".nav-main");
      if(!fix_menu) {return;}
      fix_drop = fix_menu.querySelectorAll(".nav-main .dropdown");

      [].forEach.call(fix_drop, function(element, index){
          if(isTouch()){
              element.addEventListener('touchstart', toggleDropdown, false);
              element.addEventListener('click', toggleDropdown, false);
          } else {
              element.addEventListener('mouseenter', toggleDropdown, false);
              element.addEventListener('mouseleave', toggleDropdown, false);
          }            
      });
      
      function toggleDropdown(event){
          var self = this,
              wrapMenu = document.querySelector(".header__wrap-menu"),
              rectBoundDropdownMenu = 0,
              rectBoundWrapMenu = 0,
              dropdownMenu;

          dropdownMenu = self.querySelector(".dropdown-menu");
          dropdownMenu.style.cssText = "display: block;";
          rectBoundDropdownMenu = dropdownMenu.getBoundingClientRect();
          rectBoundWrapMenu = wrapMenu.getBoundingClientRect();
          dropdownMenu.style.cssText = "";

          if(rectBoundWrapMenu.right < rectBoundDropdownMenu.right){
              dropdownMenu.style.cssText = "display: block;";
              rectBoundDropdownMenu = dropdownMenu.getBoundingClientRect();
              dropdownMenu.style.display = "";
              if(rectBoundWrapMenu.left > rectBoundDropdownMenu.left){
                  dropdownMenu.style.cssText = "margin-right: " + (rectBoundDropdownMenu.left - rectBoundWrapMenu.left) + "px";
              }
          }
          if(event.type === 'click'){
              var target = event.target;
              if(target.classList.contains("dropdown-toggle")){
                  event.stopPropagation();
                  event.preventDefault();
                  return false;
              }
          } else if(event.type === 'mouseenter'){
              dropdownMenu.classList.add("show");
              self.classList.add("show");
              self.setAttribute("aria-expanded", "true");
          } else if(event.type === 'mouseleave'){
              dropdownMenu.classList.remove("show");
              self.classList.remove("show");
              self.setAttribute("aria-expanded", "false");
          } else if(event.type === 'touchstart'){
              var target = event.target;
              var toggleLink = self.querySelector("a"),
                  parentDropdownMenu = self.closest(".dropdown-menu");
               [].forEach.call(dropdowns, function(element, index){
                  if(self !== element && !(parentDropdownMenu && self !== parentDropdownMenu)){
                      element.classList.remove("show");
                      element.querySelector(".dropdown-menu").classList.remove("show");
                      element.querySelector("a").setAttribute("aria-expanded", "false");
                  }
              }); 
              if(target === toggleLink){
                  if(toggleLink.getAttribute("aria-expanded") === "false"){
                      dropdownMenu.classList.add("show");
                      self.classList.add("show");
                      toggleLink.setAttribute("aria-expanded", "true");
                  } else {
                      dropdownMenu.classList.remove("show");
                      self.classList.remove("show");
                      toggleLink.setAttribute("aria-expanded", "false");
                  }
                  event.stopPropagation();
                  event.preventDefault();
                  return false;
              } 
          }
      }
      
      //close  menu click other element
      document.addEventListener('click', function (event) {
        var activeFixDropdown = fix_menu.querySelector(".nav-main > .dropdown.show");
        if (activeFixDropdown && !activeFixDropdown.contains(event.target)) {
          [].forEach.call(fix_drop, function (element, index) {
            element.classList.remove("show");
            element.querySelector(".dropdown-menu").classList.remove("show");
            element.querySelector("a").setAttribute("aria-expanded", "false");
          });
        }
      });
      document.addEventListener('click', function(event) {
          var activeDropdown = menu.querySelector(".nav-main > .dropdown.show");
          if (activeDropdown && !activeDropdown.contains(event.target)) {
              [].forEach.call(dropdowns, function(element, index){
                  element.classList.remove("show");
                  element.querySelector(".dropdown-menu").classList.remove("show");
                  element.querySelector("a").setAttribute("aria-expanded", "false");
              });
          }
      });
  }

  function _activeOffset(){
    $('.login-link, .product, .feedback-link, .items-order, .question-btn, .get-gift').click(function () {
      var scW = _getScrollbarWidth();
      $(".overflow-fix-m").css({ "marginRight": scW });
      $(".overflow-fix-p").css({ "paddingRight": scW });
      $(".overflow-fix-pos").css({ "right": scW });
    });
    $('.popup-close').click(function(){
      var scW = _getScrollbarWidth();
      $(".overflow-fix-m").css({ "marginRight": '' });
      $(".overflow-fix-p").css({ "paddingRight": '' });
      $(".overflow-fix-pos").css({ "right": '' });
    });
  };

  var pageOverflow = {};
  pageOverflow.init = function(){
      var body = document.getElementsByTagName("body"); 
      var scW = _getScrollbarWidth();
      $(".overflow-fix-m").css({ "marginRight": scW });
      $(".overflow-fix-p").css({ "paddingRight": scW });
      $(".overflow-fix-pos").css({ "right": scW });
      body[0].classList.add("page_overflow");
  };
  pageOverflow.destroy = function(){
      var body = document.getElementsByTagName("body"); 
      $(".overflow-fix-m").css({ "marginRight": '' });
      $(".overflow-fix-p").css({ "paddingRight": '' });
      $(".overflow-fix-pos").css({ "right": '' });
      body[0].classList.remove("page_overflow");
  };

  function _getScrollbarWidth() {
      var outer = document.createElement("div");
      outer.style.visibility = "hidden";
      outer.style.width = "100px";
      outer.style.msOverflowStyle = "scrollbar"; 
      document.body.appendChild(outer);
      var widthNoScroll = outer.offsetWidth;
      
      outer.style.overflow = "scroll";
      
      var inner = document.createElement("div");
      inner.style.width = "100%";
      outer.appendChild(inner);        
      var widthWithScroll = inner.offsetWidth;
      
      outer.parentNode.removeChild(outer);
      return widthNoScroll - widthWithScroll;
  }

  function loadScript(url, callback){
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.async = true;
    if (script.readyState){ 
      script.onreadystatechange = function(){
        if (script.readyState === "loaded" ||
            script.readyState === "complete"){
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      script.onload = function(){
        callback();
      };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
  }
  
  document.addEventListener("DOMContentLoaded", function(){
    init();
  }, false);

  return{
    isTouch: isTouch,
    loadScript: loadScript
  };
}();

